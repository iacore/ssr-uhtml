import initSsr from '../uhtml/esm/init-ssr.js'
const { html, render, document } = initSsr()

let count = 0;

export function render_page() {
    render(document.body, html`Hello from JS! This route has been hit ${++count} time(s).`)
    return document.toString()
}
