const std = @import("std");
const httpz = @import("httpz");

const Global = struct {
    l: std.Thread.Mutex = .{},
    rt: *c.JSRuntime,
    ctx: *c.JSContext,
    render_func: c.JSValue,

    /// render index page
    pub fn page_index(this: *Global, _: *httpz.Request, res: *httpz.Response) !void {
        this.l.lock(); // sadly,
        defer this.l.unlock(); // "Inside a given runtime, no multi-threading is supported."

        var argv = [_]c.JSValue{};
        const s = c.JS_Call(this.ctx, this.render_func, .{ .tag = c.JS_TAG_UNDEFINED }, 0, &argv);
        defer c.JS_FreeValue(this.ctx, s);

        // or, more verbose: httpz.ContentType.TEXT
        res.content_type = .HTML;
        // res.body =
    }
};

pub fn main() !u8 {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    const render_func = c.JS_GetPropertyStr(ctx, mod_awawa, "render_page");
    const str = c.JS_ToCString(ctx, mod_awawa);
    std.debug.print("{s}\n", .{str});

    defer c.JS_FreeValue(ctx, render_func);
    std.debug.print("{}\n", .{render_func});
    std.debug.assert(c.JS_IsFunction(ctx, render_func) != 0);

    var global = Global{
        .rt = rt,
        .ctx = ctx,
        .render_func = render_func,
    };
    var server = try httpz.ServerCtx(*Global, *Global).init(allocator, .{}, &global);
    var router = server.router();
    router.get("/", Global.page_index);
    std.debug.print("listening on http://localhost:{}/\n", .{server.config.port.?});
    try server.listen();
    return 0;
}
