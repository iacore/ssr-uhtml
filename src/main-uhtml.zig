const std = @import("std");
const httpz = @import("httpz");
const c = @cImport({
    @cInclude("quickjs.h");
    @cInclude("quickjs-libc.h");
});

const Global = struct {
    l: std.Thread.Mutex = .{},
    rt: *c.JSRuntime,
    ctx: *c.JSContext,
    render_func: c.JSValue,

    /// render index page
    pub fn page_index(this: *Global, _: *httpz.Request, res: *httpz.Response) !void {
        this.l.lock(); // sadly,
        defer this.l.unlock(); // "Inside a given runtime, no multi-threading is supported."

        var argv = [_]c.JSValue{};
        const s = c.JS_Call(this.ctx, this.render_func, .{ .tag = c.JS_TAG_UNDEFINED }, 0, &argv);
        defer c.JS_FreeValue(this.ctx, s);

        // or, more verbose: httpz.ContentType.TEXT
        res.content_type = .HTML;
        // res.body =
    }
};

test "quickjs works or not" {
    const runtime = c.JS_NewRuntime().?;
    const context = c.JS_NewContext(runtime).?;
    c.JS_SetModuleLoaderFunc(runtime, null, c.js_module_loader, null);
    c.js_std_add_helpers(context, 0, null); // console.log etc...

    const input0 = "1 + 2";
    const val0 = c.JS_Eval(context, input0, input0.len, "(inline)", c.JS_EVAL_TYPE_GLOBAL | c.JS_EVAL_FLAG_STRICT | c.JS_EVAL_FLAG_STRIP);
    var val0_: i32 = undefined;
    const success = c.JS_ToInt32(context, &val0_, val0);
    std.debug.assert(success == 0);
    std.debug.assert(val0_ == 3);
}

pub fn main() !u8 {
    const rt = c.JS_NewRuntime().?;
    defer c.JS_FreeRuntime(rt);
    const ctx = c.JS_NewContext(rt).?;
    defer c.JS_FreeContext(ctx);
    c.JS_SetModuleLoaderFunc(rt, null, c.js_module_loader, null);
    c.js_std_add_helpers(ctx, 0, null); // console.log etc...

    var input1_len: usize = undefined;
    const input1 = c.js_load_file(ctx, &input1_len, "uhtml/dom.js");
    const load_mod_promise = c.JS_Eval(ctx, input1, input1_len, "uhtml/dom.js", c.JS_EVAL_TYPE_MODULE | c.JS_EVAL_FLAG_STRICT | c.JS_EVAL_FLAG_STRIP);
    {
        defer c.JS_FreeValue(ctx, load_mod_promise);
        std.debug.print("{}\n", .{load_mod_promise});
        if (c.JS_IsException(load_mod_promise) != 0) {
            c.js_std_dump_error(ctx);
            return 1;
        }
    }

    // const input2 =
    //     \\loadScript("uhtml/dom.js")
    // ;
    // const aa = c.JS_Eval(ctx, input2, input2.len, "(inline)", c.JS_EVAL_FLAG_STRICT | c.JS_EVAL_FLAG_STRIP);
    // {
    //     defer c.JS_FreeValue(ctx, aa);
    //     std.debug.print("{}\n", .{aa});
    //     if (c.JS_IsException(aa) != 0) {
    //         c.js_std_dump_error(ctx);
    //         return 1;
    //     }
    // }
    // const m = c.js_module_loader(ctx, "src/awawa.js", null);

    // var val: c.JSValue = undefined;
    // const err = c.JS_GetModuleExportValue(ctx, m, "render_page", &val);
    // if (err != 0) {
    //     return error.CannotGetModuleExportValue;
    // }
    // defer c.JS_FreeValue(ctx, val);

    // var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    // const allocator = gpa.allocator();

    // const render_func = c.JS_GetPropertyStr(ctx, mod_awawa, "render_page");
    // const str = c.JS_ToCString(ctx, mod_awawa);
    // std.debug.print("{s}\n", .{str});

    // defer c.JS_FreeValue(ctx, render_func);
    // std.debug.print("{}\n", .{render_func});
    // std.debug.assert(c.JS_IsFunction(ctx, render_func) != 0);

    // var global = Global{
    //     .rt = rt,
    //     .ctx = ctx,
    //     .render_func = render_func,
    // };
    // var server = try httpz.ServerCtx(*Global, *Global).init(allocator, .{}, &global);
    // var router = server.router();
    // router.get("/", Global.page_index);
    // std.debug.print("listening on http://localhost:{}/\n", .{server.config.port.?});
    // try server.listen();
    return 0;
}
